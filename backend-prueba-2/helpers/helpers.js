'use strict'

const math = require('mathjs')

const parseTime = time => operation => to => {
  const periods = [
    {
      period: "meses",
      days: 30,
      months: 1,
      years: 12
    },
    {
      period: "dias",
      days: 1,
      years: 360,
      months: 30,
    },
    {
      period: "años",
      days: 360,
      months: 12,
      years: 1
    }
  ]
  let currentTime = periods.find(element => time.includes(element.period))
  let numberPeriod = Number(t.replace(currentTime.period, ''))
  let operationResult = math.evaluate(`${numberPeriod} ${operation} ${currentTime[to]}`)

  return operationResult
}

//const parseR = r => r.replace('%', '')

const addTIIE = r => scope => r.includes('TIIE') ? math.evaluate(r, scope) : r

const parseHelper = data => {
  let response = {}
  ////if (data.T) { response.T = parseTime(data.T) }
  //if (data.R) { response.R = parseR(data.R) }
  if (data.T) { response.T = data.T }
  if (data.R) { response.R = data.R }
  if (data.Q) { response.Q = data.Q }

    return response
}

module.exports = {
  parseHelper,
  addTIIE
}