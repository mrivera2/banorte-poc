'use strict'

const math = require('mathjs')

let show = (req, res) => {

  let { parseData, formule } = req.inject  
  
  let Q = math.evaluate(parseData.Q)
  let R = math.evaluate(parseData.R)
  let T = math.evaluate(parseData.T)
  
  parseData.Q = Q;
  parseData.R = R;
  parseData.T = T;

  console.log(" --- parseData: " + JSON.stringify(parseData))
  console.log(" --- formule: " + formule)

  let resultExpresion = math.evaluate(formule, parseData) 
   
  res.status(200)
    .send({M: resultExpresion})
}

module.exports = {
  show
}