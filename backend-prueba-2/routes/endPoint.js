'use strict'

const express = require('express')
const router = express.Router()

const controller = require('../controllers/routeController')
const middleware = require('../middlewares/routeMiddleware')

router.post('/', 
  middleware.validateBody,
  middleware.injectTIIEValues,
  middleware.injectFormule,
  middleware.validateData,
  controller.show
)


module.exports = router