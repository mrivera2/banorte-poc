'use strict'

const { parseHelper, addTIIE } = require('../helpers/helpers')

let injectTIIEValues = (req, res, next) => {
  /*
  req.inject.TTIE28 = 8.9000,
  req.inject.TTIE91 = 8.9000,
  req.inject.TTIE182 = 8.9000
  */
  next()
}

let validateData = (req, res, next) => {
  /*req.inject.parseData = parseHelper(req.inject.data)
  req.inject.parseData.TIIE28 = 8.495000,
  req.inject.parseData.TIIE91 = 8.495000,
  req.inject.parseData.TIIE182 = 8.495000
  req.inject.parseData.R = addTIIE(req.inject.parseData.R)(req.inject.parseData)
  */
  next()
}

let validateBody = (req, res, next) => {
  try {
    let { formule } = req.body
    let [formule_, data] = formule.split('data:')

    // Paso 1 Hacer minusculas la cadena
    formule_ = formule_.toUpperCase()
    data = data.toUpperCase()

    // Paso 2 Remplazar : por = en la cadena
    formule_ = formule_.replace(/:/g,'=')
    data = data.replace(/:/g,'=')

    // Paso 3 Eliminar los espacios en la cadena
    formule_ = formule_.replace(/ /g,'')
    data = data.replace(/ /g,'')

    // Paso 4 Dejar limpia la funcion a evaluar
    formule_ = formule_.replace('FORMULE=','')
    formule_ = formule_.replace(',','')
    var form = formule_.split('=')
    formule_ = form[1]

    // Paso 5 limpiar los datos 
    data = data.replace('{','')
    data = data.replace('}','')
    data = data.replace('MESES','/12')    
    data = data.replace('%','/100')

    var dataMap = {};
    data.split(',').forEach(function(value){
      var vals = value.split('=');
      dataMap[vals[0]] = vals[1]
    });

    data = dataMap;
    req.inject.data = data
    req.inject.formule = formule_
    next()
  } catch (err) {
    console.log('Error parseando los datos')
    console.log(err)
    return res
      .status(400)
      .send({message: err})
  } 
}

let injectFormule = (req, res, next) => {
  /*
  let { formule } = req.inject
  let formuleType = [
    { 
      formule: "Q*(1+R/100*T/360)",
      type: 'saldo_intereses_simples'
    },
    { 
      formule: "(Q*T*(R))+Q",
      type: 'prestamo'
    },
    { 
      formule: "(sum(Q)/D)*(R/12)",
      type: 'intereses_tarjeta_credito'
    },
    { 
      formule: "log(Q/C)/log(1 + R)",
      type: 'tiempo_inversion_monto_bimestral'
    },
    { 
      formule: "tiempo",
      type: 'tiempo'
    }
  ]

  let currentFormule = formuleType.find(element => formule.includes(element.formule))

  if (!currentFormule) {
    return res
      .status(400)
      .send({message: ''})
  }
  req.inject.currentFormule = currentFormule
  */
  next()
}

module.exports = {
  injectTIIEValues,
  injectFormule,
  validateData,
  validateBody
}