'use stric'

const express = require('express')
const bodyParser = require('body-parser')
const route = require('./routes/endPoint')
const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: false}))
app.use((req, res, next) => {
  req.inject = req.inject ||  {}
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method')
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE')
  res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE')
  next()
})
app.use('/route', route)

app.listen(3000, () => {
  console.log('Servidor escuchando sobre el puerto ' + 3000)
})